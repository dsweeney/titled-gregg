from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .scraper import random_gregg
from .image_processing import random_distort

def index(request):
    gregg = random_gregg()
    path = random_distort(gregg)

    template = loader.get_template('gregg.html')
    context = {
        'path': path
    }
    return HttpResponse(template.render(context, request))