from django.apps import AppConfig


class TiltedConfig(AppConfig):
    name = 'tilted'
