from requests import get
from requests.exceptions import RequestException
from contextlib import closing
from bs4 import BeautifulSoup
import random

gregg_images_link = "https://www.google.com/search?q=gregg+wallace&source=lnms&tbm=isch&sa=X&ved=0ahUKEwjZoaiGnp3hAhXCWxUIHeBrBn4Q_AUIDigB&biw=1280&bih=607"

def random_gregg():
    page = simple_get(gregg_images_link)
    images = get_images(page)

    desired_gregg = images[random.randint(0, len(images)-1)]

    return desired_gregg['src']

def get_images(page):
    html = BeautifulSoup(page, 'html.parser')

    images = html.select('table.images_table')[0].select('img')

    return images

def simple_get(url):
    """
    Attempts to get the content at `url` by making an HTTP GET request.
    If the content-type of response is some kind of HTML/XML, return the
    text content, otherwise return None.
    """
    try:
        with closing(get(url, stream=True)) as resp:
            if is_good_response(resp):
                return resp.content
            else:
                return None

    except RequestException as e:
        log_error('Error during requests to {0} : {1}'.format(url, str(e)))
        return None


def is_good_response(resp):
    """
    Returns True if the response seems to be HTML, False otherwise.
    """
    content_type = resp.headers['Content-Type'].lower()
    return (resp.status_code == 200 
            and content_type is not None 
            and content_type.find('html') > -1)


def log_error(e):
    """
    It is always a good idea to log errors. 
    This function just prints them, but you can
    make it do anything.
    """
    print(e)