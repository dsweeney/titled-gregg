import urllib.request
import cv2
import numpy as np
import random

def random_distort(url):
    path = 'temp.png'
    image = url_to_image(url)
    distorted = image

    distorted = resize(distorted, 2)
    distorted = warp(distorted)
    distorted = resize(distorted, 2)

    cv2.imwrite(path, distorted)

    return path

def warp(image):
    rows,cols,ch = image.shape

    pts1 = np.float32(
        [[cols*random.uniform(0.50, 0.75), rows*random.uniform(0.75, 1.25)],
        [cols*random.uniform(0.80, 1.2), rows*random.uniform(0.75, 1.25)],
        [cols*.10, 0],
        [cols,     0]]
    )
    pts2 = np.float32(
        [[cols*0.1, rows],
        [cols,     rows],
        [0,        0],
        [cols,     0]]
    )    

    M = cv2.getPerspectiveTransform(pts1,pts2)
    distorted = cv2.warpPerspective(image,M,(cols,rows))

    return distorted

def resize(image, factor):
    return cv2.resize(image,None,fx=factor, fy=factor, interpolation = cv2.INTER_CUBIC)

def url_to_image(url):
    print("url: " + url)
    resp = urllib.request.urlopen(url)
    image = np.asarray(bytearray(resp.read()), dtype="uint8")
    image = cv2.imdecode(image, cv2.IMREAD_COLOR)
    return image